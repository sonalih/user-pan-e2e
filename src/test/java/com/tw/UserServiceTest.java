package com.tw;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static io.restassured.RestAssured.*;
import static org.assertj.core.api.Assertions.assertThat;

public class UserServiceTest {

    RestAssured restAssured;
    @Test
    void shouldAssertTrueIsTrue() {
        assertThat(true).isTrue();
    }

    @Test
    void shouldReturnSuccessResponse() throws IOException {
        User user = new User("foow.com", "ASDFGH");
        with()
                .body(new ObjectMapper().writeValueAsString(user))
                .contentType("application/json")
                .when().request(Method.POST,"http://localhost:9080/users").then().statusCode(200);
    }
}
